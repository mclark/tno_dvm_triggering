# TNO_DVM_Triggering

## Name
TNO DVM Triggering Template

## Description
Template for creating a a g-code program that will perform a linear motion.
The motion will ramp up, then start triggering, and then ramp down without triggers.

## Usage
There are 8 configurable R-Parameters:
R61-R66 are the start and end co-ordinates.
R67 is the distance between triggers (Default 1mm)
R72 is the Ramp-Up/Ramp-Down distance (Default 20mm)
